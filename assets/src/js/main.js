/**
 * Created by tam.nguyen-huu on 4/5/2018.
 */

"use strict";
// Navigation menu button
function navAction() {
    document.getElementById("dropdown").classList.toggle("show");
}

window.onclick = function (e) {
    if (!e.target.matches('.menu-icon')) {
        var myDropdown = document.getElementById("dropdown");
        if (myDropdown.classList.contains('show')) {
            myDropdown.classList.remove('show');
        }
    }
};

window.addEventListener('click', function (e) {
    if (document.getElementById('navTrigger').contains(e.target)) {
        document.getElementById("dropdown").classList.toggle("show");
    }
});

// Scroll to a specific ID
window.letScroll = function (target) {
    var scrollContainer = target;
    do {
        scrollContainer = scrollContainer.parentNode;
        if (!scrollContainer) {
            return;
        }
        scrollContainer.scrollTop += 1;
    } while (scrollContainer.scrollTop === 0);

    var targetY = 0;
    do {
        if (target === scrollContainer) {
            break;
        }
        targetY += target.offsetTop;
    } while (target = target.offsetParent);

    var scroll = function (c, a, b, i) {
        i++;
        if (i > 30) {
            return;
        }
        c.scrollTop = a + (b - a) / 30 * i;
        setTimeout(function () {
            scroll(c, a, b, i);
        }, 15);
    };

    scroll(scrollContainer, scrollContainer.scrollTop, targetY - 60, 0);
};


// Go to top
var stepTime = 20;
var docBody = document.body;
var focElem = document.documentElement;

var scrollAnimationStep = function (initPos, stepAmount) {
    var newPos = initPos - stepAmount > 0 ? initPos - stepAmount : 0;

    docBody.scrollTop = focElem.scrollTop = newPos;

    newPos && setTimeout(function () {
        scrollAnimationStep(newPos, stepAmount);
    }, stepTime);
};

var scrollTopAnimated = function (speed) {
    var topOffset = docBody.scrollTop || focElem.scrollTop;
    var stepAmount = topOffset;

    speed && (stepAmount = (topOffset * stepTime) / speed);

    scrollAnimationStep(topOffset, stepAmount);
};

window.onscroll = function () {
    scrollFunction();
};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("scrollTop").style.display = "block";
    } else {
        document.getElementById("scrollTop").style.display = "none";
    }

}