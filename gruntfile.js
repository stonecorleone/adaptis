'use strict';

module.exports = function (grunt) {
    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);
    var appConfig = {
        dist: 'assets/dist/',
        src: 'assets/src/'
    };

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        envConfig: appConfig,

        clean: {
            subfolders: '<%= envConfig.dist %>/*/'
        },

        watch: {
            resource: {
                files: [
                    '<%= envConfig.src %>/**/*.*'
                ],
                tasks: ['copy'],
                options: {
                    spawn: false
                }
            },
            sass: {
                files: ['<%= envConfig.src %>/css/*.sass'],
                tasks: ['scu'],
                options: {
                    spawn: false
                }
            },
            js: {
                files: ['<%= envConfig.src %>/js/*.js'],
                tasks: ['uglify','copy'],
                options: {
                    spawn: false
                }
            },
            gruntfile: {
                files: ['gruntfile.js'],
                tasks: ['build']
            }
        },

        // Sass object
        sass: {
            dist: {
                files: [{
                    cwd: '<%= envConfig.src %>/css',
                    dest: '<%= envConfig.dist %>/css',
                    src: ['*.sass'],
                    ext: '.css',
                    expand: true
                }],
                options: {
                    style: 'expanded',
                    compass: true
                }
            }
        },

        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: '<%= envConfig.dist %>/css',
                    src: ['*.css', '!*.min.css'],
                    dest: '<%= envConfig.dist %>/css/',
                    ext: '.min.css'
                }]
            }
        },

        uglify: {
            options: {
                manage: false
            },
            target: {
                files: [{
                    expand: true,
                    cwd: '<%= envConfig.src %>/js',
                    src: ['*.js', '!*.min.js'],
                    dest: '<%= envConfig.dist %>/js/',
                    ext: '.min.js'
                }]
            }
        },

        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: {
                src: [
                    'gruntfile.js',
                    '<%= envConfig.src %>/js/*.js'
                ]
            }
        },

        copy: {
            resource: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= envConfig.src %>',
                    dist: '<%= envConfig.dist %>',
                    src: [
                        'js/*.*',
                        'css/*.*'
                    ]
                }]
            }
        },
        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ['last 1 version']
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/css/',
                    src: '{,*/}*.css',
                    dest: '.tmp/css/'
                }]
            }
        }
    });

    grunt.registerTask('build',[
        'clean',
        'copy'
    ]);

    grunt.registerTask('scu', [
        'sass',
        'cssmin',
        'uglify',
        //'jshint',
        'autoprefixer'
    ]);

    grunt.registerTask('default',[
        'build',
        'scu',
        'watch'
    ]);
};